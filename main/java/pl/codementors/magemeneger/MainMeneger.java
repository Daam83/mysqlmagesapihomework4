package pl.codementors.magemeneger;

import pl.codementors.magemeneger.Menegers.*;
import pl.codementors.magemeneger.models.SpellBooks;
import pl.codementors.magemeneger.models.Wands;

import java.util.Scanner;

/**
 * Created by student on 07.07.17.
 */
public class MainMeneger {
    /*
    * This class is the main class with main metod to
     */
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        CoreMeneger coreMeneger = new CoreMeneger();
        MagesMeneger magesMeneger = new MagesMeneger();
        SpellBooksMeneger spellBooksMeneger = new SpellBooksMeneger();
        WandMeneger wandMeneger = new WandMeneger();
        SpellsMeneger spellsMeneger = new SpellsMeneger();
        WoodMeneger woodMeneger = new WoodMeneger();
        /*
        * @param: "run" given only for param  for comunication  with user its a simple user
         */
        String command;
        boolean run = true;
        System.out.println("Welcome to main menu of Base of Mage.");

        while (run) {

            System.out.println("please choose: Mage , Spell, Wand, SpellBooks, Wood, Core, quit");
            System.out.print("command: ");
            command = scanner.next();

            switch (command) {
                case "Core": {
                    System.out.println("Welcome to the menu of " + command);
                    coreMeneger.manage(scanner);

                    break;
                }
                case "Wood": {
                    System.out.println("Welcome to the menu of " + command);
                    woodMeneger.manage(scanner);
                    break;
                }
                case "SpellBooks": {
                    System.out.println("Welcome to the menu of " + command);
                    spellBooksMeneger.manage(scanner);
                    break;
                }
                case "Wand": {
                    System.out.println("Welcome to the menu of " + command);
                    wandMeneger.manage(scanner);
                    break;
                }
                case "Spell": {
                    System.out.println("Welcome to the menu of " + command);
                    spellsMeneger.manage(scanner);
                    break;
                }
                case "Mage": {
                    System.out.println("Welcome to the menu of " + command);
                    magesMeneger.manage(scanner);
                    break;
                }
                case "quit": {
                    System.out.println("Bye Bye:(");
                    run = false;
                    break;
                }
                default: {
                    System.out.println("wrong command");
                }
            }
        }
    }
}

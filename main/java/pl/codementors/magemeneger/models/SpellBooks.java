package pl.codementors.magemeneger.models;
import lombok.*;

import java.util.Date;
import java.util.List;

/**
 * Created by student on 05.07.17.
 */
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@EqualsAndHashCode
@ToString
public class SpellBooks {

    private int id;
    private String title;
    private String author;
    private Date publishDate;
    private List<Spells> spells;

    // Construtor to set not all arguments

    public SpellBooks(String title, String author, Date publishDate){
        this.title=title;
        this.author=author;
        this.publishDate=publishDate;
    }
    public SpellBooks(int id,String title, String author, Date publishDate){
        this.id=id;
        this.title=title;
        this.author=author;
        this.publishDate=publishDate;
    }
}

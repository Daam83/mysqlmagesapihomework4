package pl.codementors.magemeneger.models;


import lombok.*;

/**
 * Created by student on 04.07.17.
 */
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@EqualsAndHashCode
@ToString
public class Core {
    private int id;
    private String name;
    private int power;
    private int consistency;

    // Construtor to set not all arguments


    public Core(String name, int power, int cosistency){
        this.name=name;
        this.power=power;
        this.consistency = cosistency;
    }


}

package pl.codementors.magemeneger.models;
import lombok.*;

import java.util.List;

/**
 * Created by student on 05.07.17.
 */

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@EqualsAndHashCode
@ToString
public class Mages {
    private int id;
    private String name;
    private Wands wand;
    private Mages supervisior;
    private List<Spells> spells;

    // Constructor to set not all arguments

    public Mages(String name, Wands wand) {
        this.name = name;
        this.wand = wand;
    }

    public Mages(String name, Wands wand, Mages supervisior){
        this.name=name;
        this.wand=wand;
        this.supervisior=supervisior;
    }

    public Mages(String name, Wands wand, Mages supervisior, List<Spells> spells) {
        this.name = name;
        this.wand = wand;
        this.supervisior = supervisior;
        this.spells = spells;
    }
    public Mages(int id, String name, Wands wand, Mages supervisior){
        this.id=id;
        this.name=name;
        this.wand=wand;
        this.supervisior=supervisior;
    }
//    public Mages(int id, String name, Wands wand){
//        this.id=id;
//        this.name=name;
//        this.wand=wand;
//           }

}

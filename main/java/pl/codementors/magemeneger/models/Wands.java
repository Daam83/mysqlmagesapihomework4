package pl.codementors.magemeneger.models;
import lombok.*;

import java.util.Date;

/**
 * Created by student on 04.07.17.
 */
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@EqualsAndHashCode
@ToString
public class Wands {
    private int id;
    private Woods wood;
    private Core core;
    private Date productionDate;

    // Construtor to set not all arguments


    public Wands(Woods wood, Core core, Date productionDate){
        this.wood=wood;
        this.core=core;
        this.productionDate=productionDate;
    }



}

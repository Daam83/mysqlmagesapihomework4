package pl.codementors.magemeneger.models;
import lombok.*;

/**
 * Created by student on 04.07.17.
 */
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@EqualsAndHashCode
@ToString
public class Woods {
    private int id;
    private String name;
    private int toughness;

    // Construtor to set not all arguments. this constructor will be use to create exeqite


    public Woods(String name,int toughness){
        this.name=name;
        this.toughness=toughness;

    }


}

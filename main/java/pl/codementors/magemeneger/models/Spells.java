package pl.codementors.magemeneger.models;

import lombok.*;

/**
 * Created by student on 05.07.17.
 */
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@EqualsAndHashCode
@ToString
public class Spells {
    private int id;
    private String incantation;

    // Construtor to set not all arguments


    public Spells(String incantation){
        this.incantation=incantation;
    }



}

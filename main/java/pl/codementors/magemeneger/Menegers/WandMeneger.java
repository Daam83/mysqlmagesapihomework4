package pl.codementors.magemeneger.Menegers;

import pl.codementors.magemeneger.ServiceData.BaseDAO;
import pl.codementors.magemeneger.ServiceData.CoreDAO;
import pl.codementors.magemeneger.ServiceData.WandsDAO;
import pl.codementors.magemeneger.ServiceData.WoodsDAO;
import pl.codementors.magemeneger.models.Core;
import pl.codementors.magemeneger.models.Wands;
import pl.codementors.magemeneger.models.Woods;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Scanner;
import java.util.TimeZone;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Created by student on 07.07.17.
 */
public class WandMeneger extends BaseMeneger<Wands, WandsDAO> {


    public WandMeneger() {
        dao = new WandsDAO();
    }

    public static final Logger log = Logger.getLogger(BaseDAO.class.getName());

    // metod paseNew is used in add metod and uptede metod from baseDao
    @Override
    protected Wands parseNew(Scanner scanner) {
        System.out.println(" 'NEW Wands' please give:");
        System.out.print("new Woods id: ");
        int woodId = scanner.nextInt();
        System.out.print("new Core id: ");
        int coreId = scanner.nextInt();
        System.out.println("new Production Date: ");
        WoodsDAO dao = new WoodsDAO();
        Woods wood = dao.find(woodId);
        CoreDAO dao1 = new CoreDAO();
        Core core = dao1.find(coreId);
        scanner.skip("\n");
        System.out.print("new production in format(yyyy-MM-dd): ");
        String birthDay = scanner.nextLine();
        SimpleDateFormat output = new SimpleDateFormat("yyyy-MM-dd");
        output.setTimeZone(TimeZone.getTimeZone("GMT +2"));
        Date date = new Date();
        try {
            date = output.parse(birthDay);
        } catch (ParseException e) {
            log.log(Level.WARNING, e.getMessage(), e);
        }


        return new Wands(wood, core, date);
    }

    @Override
    protected void copyId(Wands from, Wands to) {
        to.setId(from.getId());
    }
}



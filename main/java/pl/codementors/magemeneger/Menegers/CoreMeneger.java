package pl.codementors.magemeneger.Menegers;

import pl.codementors.magemeneger.ServiceData.CoreDAO;
import pl.codementors.magemeneger.models.Core;

import java.util.Scanner;


/**
 * Created by student on 07.07.17.
 */
public class CoreMeneger extends BaseMeneger<Core, CoreDAO> {

    public CoreMeneger() {
        dao = new CoreDAO();
    }

    // metod paseNew is used in add metod and uptede metod from baseDao
    @Override
    protected Core parseNew(Scanner scanner) {
        System.out.println(" 'NEW CORE' please give:");
        System.out.print("new name: ");
        String name = scanner.next();
        System.out.print("new power: ");
        int power = scanner.nextInt();
        System.out.print("new cosistency: ");
        int cosistency = scanner.nextInt();

        return new Core(name, power, cosistency);
    }

    @Override
    protected void copyId(Core from, Core to) {
        to.setId(from.getId());
    }
}



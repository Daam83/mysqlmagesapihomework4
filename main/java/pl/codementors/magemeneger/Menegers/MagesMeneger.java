package pl.codementors.magemeneger.Menegers;

import pl.codementors.magemeneger.ServiceData.MagesDAO;
import pl.codementors.magemeneger.ServiceData.WandsDAO;
import pl.codementors.magemeneger.models.Mages;
import pl.codementors.magemeneger.models.Spells;
import pl.codementors.magemeneger.models.Wands;

import java.util.List;
import java.util.Scanner;

/**
 * Created by student on 07.07.17.
 */
public class MagesMeneger extends BaseMeneger<Mages, MagesDAO> {

    public MagesMeneger() {
        dao = new MagesDAO();
    }

    // metod paseNew is used in add metod and uptede metod from baseDao
    @Override
    protected Mages parseNew(Scanner scanner) {
        System.out.println(" 'NEW Mage' please give:");
        System.out.print("new name: ");
        scanner.skip("\n");
        String name = scanner.nextLine();
        System.out.print("new wandsID: ");
        int wandsID = scanner.nextInt();
        WandsDAO dao = new WandsDAO();
        Wands wand = dao.find(wandsID);
        System.out.println("new Mage SupervisiorID:");
        int superID = scanner.nextInt();
        MagesDAO dao1 = new MagesDAO();
        Mages supervirior = dao1.find(superID);
        return new Mages(name, wand, supervirior);
    }

    @Override
    protected void copyId(Mages from, Mages to) {
        to.setId(from.getId());
    }

}





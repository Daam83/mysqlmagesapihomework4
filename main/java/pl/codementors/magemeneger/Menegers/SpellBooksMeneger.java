package pl.codementors.magemeneger.Menegers;

import pl.codementors.magemeneger.ServiceData.BaseDAO;
import pl.codementors.magemeneger.ServiceData.SpellBooksDAO;
import pl.codementors.magemeneger.models.SpellBooks;
import pl.codementors.magemeneger.models.Spells;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Scanner;
import java.util.TimeZone;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Created by student on 07.07.17.
 */
public class SpellBooksMeneger extends BaseMeneger<SpellBooks, SpellBooksDAO> {


    public SpellBooksMeneger() {
        dao = new SpellBooksDAO();
    }

    public static final Logger log = Logger.getLogger(BaseDAO.class.getName());

    // metod paseNew is used in add metod and uptede metod from baseDao
    @Override
    protected SpellBooks parseNew(Scanner scanner) {
        System.out.println(" 'NEW Spell Book' please give:");
        System.out.print("new Title: ");
        scanner.skip("\n");
        String title = scanner.nextLine();
        scanner.skip("\n");
        System.out.print("new Author: ");
        String author = scanner.nextLine();
        scanner.skip("\n");
        System.out.print("new Publih Date in format(yyyy-MM-dd): ");
        String birthDay = scanner.nextLine();
        SimpleDateFormat output = new SimpleDateFormat("yyyy-MM-dd");
        output.setTimeZone(TimeZone.getTimeZone("GMT +2"));
        Date date = new Date();
        try {
            date = output.parse(birthDay);
        } catch (ParseException e) {
            log.log(Level.WARNING, e.getMessage(), e);
        }


        return new SpellBooks(title, author, date);
    }


    @Override
    protected void copyId(SpellBooks from, SpellBooks to) {
        to.setId(from.getId());
    }
}

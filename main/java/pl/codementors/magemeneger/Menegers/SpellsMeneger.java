package pl.codementors.magemeneger.Menegers;

import pl.codementors.magemeneger.ServiceData.SpellsDAO;
import pl.codementors.magemeneger.models.Spells;

import java.util.Scanner;

/**
 * Created by student on 07.07.17.
 */
public class SpellsMeneger extends BaseMeneger<Spells, SpellsDAO> {

    // metod paseNew is used in add metod and uptede metod from baseDao
    public SpellsMeneger() {
        dao = new SpellsDAO();
    }

    @Override
    protected Spells parseNew(Scanner scanner) {
        System.out.println(" 'NEW Spells' please give:");
        System.out.print("new incantation: ");
        String name = scanner.nextLine();
        return new Spells(name);
    }


    @Override
    protected void copyId(Spells from, Spells to) {
        to.setId(from.getId());
    }
}


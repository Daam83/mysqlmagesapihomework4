package pl.codementors.magemeneger.Menegers;

import pl.codementors.magemeneger.ServiceData.WoodsDAO;
import pl.codementors.magemeneger.models.Woods;

import java.util.Scanner;

/**
 * Created by student on 07.07.17.
 */
public class WoodMeneger extends BaseMeneger<Woods, WoodsDAO> {

    public WoodMeneger() {
        dao = new WoodsDAO();
    }
    // metod paseNew is used in add metod and uptede metod from baseDao

    @Override
    protected Woods parseNew(Scanner scanner) {
        System.out.println(" 'NEW Woods' please give:");
        System.out.print("new name: ");
        String name = scanner.next();
        System.out.print("new toughness: ");
        int toughness = scanner.nextInt();

        return new Woods(name, toughness);
    }


    @Override
    protected void copyId(Woods from, Woods to) {
        to.setId(from.getId());
    }
}


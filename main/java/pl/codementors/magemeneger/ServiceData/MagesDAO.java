package pl.codementors.magemeneger.ServiceData;

import pl.codementors.magemeneger.models.Mages;
import pl.codementors.magemeneger.models.Spells;
import pl.codementors.magemeneger.models.Wands;
import pl.codementors.magemeneger.ServiceData.SpellsDAO;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;

/**
 * Created by student on 06.07.17.
 */
public class MagesDAO extends BaseDAO<Mages> {
     /*
    * Columns witch are show must be on same as on DataBase table mages
     */
     private String[] columns = {"name", "wand", "supervisor"};

    //    private String[] columns={"name", "wand"};
       /*
    * @return: name of table in DataBese "mages"
     */
    @Override
    public String getTableName() {
        return "mages";
    }

    /*
    * @return: new object Mages with enteres take from DataBase
    * If mage have in date bese as supervisior them self then in API will have null because this
     */
    @Override
    public Mages parseValue(ResultSet result) throws SQLException {
        int id = result.getInt(1);
        String name = result.getString(2);
        int wandID = result.getInt(3);
        Wands wand = new WandsDAO().find(wandID);
        List<Spells> spell = mageSpells(name);
        int supervisiorID = result.getInt(4);
        if (supervisiorID != id) {
            Mages supervisior = new MagesDAO().find(supervisiorID);
            return new Mages(id, name, wand, supervisior, spell);
        } else {
            return new Mages(id, name, wand, null, spell);
        }
    }
    /*
    * @return: column with are display on line 16
     */
    @Override
    public String[] getColumns() {
        return columns;
    }

    /*
   * @return:  values of column as arrays objects
    */

    @Override
    public Object[] getColumnsValues(Mages value) {
        Object[] values = {value.getName(), value.getWand().getId(), value.getSupervisior().getId()};
        return values;
    }
    /*
     * @return:pimery key of table Mages
     */
    @Override
    public int getPrimaryKeyValue(Mages value) {
        return value.getId();
    }


    /*
    * create list of spells foe mages
    *
    *
     */


    public List<Spells> mageSpells(String mage) {
        List<Spells> values = new ArrayList<>();
        String sql = "SELECT spells.id,spells.incantation FROM spells " +
                "INNER JOIN mages_spells on mages_spells.spell=spells.id " +
                "INNER JOIN mages on mages.id=mages_spells.mage" +
                " WHERE mages.name = ('" + mage + "')";
        try (Connection con = BaseConnection.createConnection();
             PreparedStatement statement = con.prepareStatement(sql);) {
            try (ResultSet result = statement.executeQuery();) {
                while (result.next()) {
                    int id = result.getInt(1);
                    String incantation = result.getString(2);
                    Spells value = new Spells(id, incantation);
                    values.add(value);
                }
            }
        } catch (SQLException ex) {
            log.log(Level.WARNING, ex.getMessage(), ex);
        }
        return values;
    }


}


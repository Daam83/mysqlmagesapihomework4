package pl.codementors.magemeneger.ServiceData;

import org.apache.commons.dbcp2.BasicDataSource;

import java.sql.Connection;
import java.sql.SQLException;

/**
 * Created by student on 05.07.17.
 */
public class BaseConnection {
    /*
    *Class to mass connection with data base mages
     */

    private final static String URL = "jdbc:mysql://localhost:3306/mages";

    private final static String USER = "mage_admin";

    private final static String PASSWORD = "m4g34dm1n";

    private final static String PARAMS = "?useUnicode=true"
            + "&useJDBCCompliantTimezoneShift=true"
            + "&useLegacyDatetimeCode=false"
            + "&serverTimezone=UTC";

    private static BasicDataSource ds;
     /*
      * Metod wich create the connection to the base of mages.
      * @return  connection as object wich are safety close after use.
      * if metode can't take a connection they will throw SQLException
      */

    public static Connection createConnection() throws SQLException {
        if (ds == null) {
            ds = new BasicDataSource();
            ds.setUsername(USER);
            ds.setPassword(PASSWORD);
            ds.setUrl(URL + PARAMS);
        }
        Connection connection = ds.getConnection();
        return connection;
    }
}

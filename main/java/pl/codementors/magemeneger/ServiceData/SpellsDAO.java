package pl.codementors.magemeneger.ServiceData;

import pl.codementors.magemeneger.models.Spells;

import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * Created by student on 06.07.17.
 */
public class SpellsDAO extends BaseDAO<Spells> {

        /*
    * Columns witch are show must be on same as on DataBase table Spells
     */
    private  String[] columns ={"incantation"};

     /*
    * @return: name of table in DataBese "spells"
     */

    @Override
    public String getTableName() {
        return "spells";
    }

    /*
     * @return: new object Spells with enteres take from DataBase
     */
    @Override
    public Spells parseValue(ResultSet result) throws SQLException {
        int id =result.getInt(1);
        String incantation =result.getString(2);
        return new Spells(id,incantation);
    }

    /*
     * @return: column with are display on line 16
     */
    @Override
    public String[] getColumns() {
        return  columns;
    }

    /*
    * @return:  values of column as arrays objects
    */
    @Override
    public Object[] getColumnsValues(Spells value) {
        Object[] values ={value.getIncantation()};
        return values;
    }

    /*
     * @return:pimery key of table spells
     */
    @Override
    public int getPrimaryKeyValue(Spells value) {
        return value.getId();
    }
}

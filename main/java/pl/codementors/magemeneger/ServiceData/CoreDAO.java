package pl.codementors.magemeneger.ServiceData;

import pl.codementors.magemeneger.models.Core;

import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * Created by student on 06.07.17.
 */
public class CoreDAO extends BaseDAO<Core> {

     /*
    * Columns witch are show must be on same as on DataBase table core
     */

    private String[] columns = {"name", "power", "consistency"};

     /*
    * @return: name of table in DataBese "cores"
     */

    @Override
    public String getTableName(){
        return "cores";
    }
    /*
    * @return: new object Core with enteres take from DataBase
     */

    @Override
    public Core parseValue(ResultSet result) throws SQLException {
        int id = result.getInt(1);
        String name = result.getString(2);
        int power = result.getInt(3);
        int cosistency = result.getInt(4);
        return new Core(id,name,power,cosistency);
    }
     /*
    * @return: column with are display on line 17
     */

    @Override
    public String[] getColumns() {
        return columns;
    }

     /*
    * @return:  values of column as arrays objects
     */

    @Override
    public Object[] getColumnsValues(Core value) {
        Object[] values = {value.getName(), value.getPower(), value.getConsistency()};
        return values;
    }

     /*
    * @return:pimery key of table Core
     */

    @Override
    public int getPrimaryKeyValue(Core value) {
        return value.getId();
    }

}


package pl.codementors.magemeneger.ServiceData;

import pl.codementors.magemeneger.models.Core;
import pl.codementors.magemeneger.models.Wands;
import pl.codementors.magemeneger.models.Woods;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Date;

/**
 * Created by student on 06.07.17.
 */
public class WandsDAO extends BaseDAO<Wands> {
     /*
    * Columns witch are show must be the same as on DataBase table wands
     */

    private  String[] columns ={"wood", "core","produtionDate"};

     /*
    * @return: name of table in DataBese "mages"
     */

    @Override
    public String getTableName() {
        return "wands";
    }
    /*
    * @return: new object Wands with enteres take from DataBase
     */

    @Override
    public Wands parseValue(ResultSet result) throws SQLException {
        int id =result.getInt(1);
        int woodID = result.getInt(2);
        int coreID = result.getInt(3);
        Date productionDate = result.getDate(4);
        Woods wood = new WoodsDAO().find(woodID);
        Core core = new CoreDAO().find(coreID);
        return new Wands(id,wood,core,productionDate);
    }

    /*
    * @return: column with are display on line 19
     */
    @Override
    public String[] getColumns() {
        return columns;
    }

    /*
    * @return:  values of column as arrays objects
    */
    @Override
    public Object[] getColumnsValues(Wands value) {
        Object[] values = {value.getWood().getId(), value.getCore().getId(), value.getProductionDate()};
        return values;
    }

    /*
    * @return:pimery key of table Wands
    */

    @Override
    public int getPrimaryKeyValue(Wands value) {
        return value.getId();
    }
}

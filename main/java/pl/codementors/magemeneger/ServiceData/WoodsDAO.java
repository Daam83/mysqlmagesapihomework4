package pl.codementors.magemeneger.ServiceData;

import pl.codementors.magemeneger.models.Woods;

import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * Created by student on 06.07.17.
 */
public class WoodsDAO extends BaseDAO<Woods> {

      /*
    * Columns witch are show must be the same as on DataBase mages
     */

    private String[] columns ={"name","toughness"};

      /*
    * @return: name of table in DataBese "mages"
     */

    @Override
    public String getTableName() {
        return "woods";
    }

    /*
    * @return: new object Core with enteres take from DataBase
     */


    @Override
    public Woods parseValue(ResultSet result) throws SQLException {
        int id = result.getInt(1);
        String name= result.getString(2);
        int toughness = result.getInt(3);
        return new Woods(id, name, toughness);
    }
     /*
    * @return: column with are display on line 17
     */

    @Override
    public String[] getColumns() {
        return columns;
    }

     /*
    * @return:  values of column as arrays objects
     */

    @Override
    public Object[] getColumnsValues(Woods value) {
        Object[] values={value.getName(),value.getToughness()};
        return  values;
    }

     /*
    * @return:pimery key of table Woods
     */

    @Override
    public int getPrimaryKeyValue(Woods value) {
        return value.getId();
    }
}


package pl.codementors.magemeneger.ServiceData;

import pl.codementors.magemeneger.models.SpellBooks;
import pl.codementors.magemeneger.models.Spells;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.logging.Level;



/**
 * Created by student on 06.07.17.
 */
public class SpellBooksDAO extends BaseDAO<SpellBooks> {


    /*
     * Columns witch are show must be on same as on DataBase table spell_books
     */
    private  String[] columns ={"title", "author","publish_date"};

    /*
     * @return: name of table in DataBese "spells"
     */
    @Override
    public String getTableName() {
        return "spell_books";
    }

    /*
     * @return: new object Spells with enteres take from DataBase
     */
    @Override
    public SpellBooks parseValue(ResultSet result) throws SQLException {
        int id = result.getInt(1);
        String title =result.getString(2);
        String author = result.getString(3);
        Date publishDate = result.getDate(4);
        List<Spells> spells = mageSpells(author);
        return new SpellBooks(id, title, author, publishDate, spells);
    }


    /*
     * @return: column with are display on line 16
     */
    @Override
    public String[] getColumns() {
        return columns;
    }

    /*
     * @return:  values of column as arrays objects
     */
    @Override
    public Object[] getColumnsValues(SpellBooks value) {
        Object[] values = {value.getTitle(),value.getAuthor(),value.getPublishDate()};
        return values;
    }

    /*
     * @return:pimery key of table spell_books
     */
    @Override
    public int getPrimaryKeyValue(SpellBooks value) {
        return value.getId();
    }

     /*
    * create list of spells for book_spells
    *
    *
     */

    public List<Spells> mageSpells(String author) {
        List<Spells> values = new ArrayList<>();
        String sql = "SELECT spells.id,spells.incantation FROM spells " +

                "INNER JOIN spells_books on spells_books.spell=  spells.id " +
                "INNER JOIN spell_books on spell_books.id=spells_books.spell_book" +
                " WHERE spell_books.author = ('" + author + "')";

        try (Connection con = BaseConnection.createConnection();
             PreparedStatement statement = con.prepareStatement(sql);) {
            try (ResultSet result = statement.executeQuery();) {
                while (result.next()) {
                    int id = result.getInt(1);
                    String incantation = result.getString(2);
                    Spells value = new Spells(id, incantation);
                    values.add(value);
                }
            }
        } catch (SQLException ex) {
            log.log(Level.WARNING, ex.getMessage(), ex);
        }
        return values;
    }
}

DROP DATABASE IF EXISTS mages;

DROP USER IF EXISTS 'mage_admin';

CREATE DATABASE mages;

CREATE USER 'mage_admin' IDENTIFIED BY 'm4g34dm1n';

GRANT ALL ON mages.* TO 'mage_admin';

USE mages;

CREATE TABLE woods (
	id INT NOT NULL AUTO_INCREMENT,
    name VARCHAR(256),
    toughness INT,
    PRIMARY KEY (id)
);

CREATE TABLE cores (
	id INT NOT NULL AUTO_INCREMENT,
    name VARCHAR(256),
    power INT,
    consistency INT,
    PRIMARY KEY (id)
);

CREATE TABLE wands (
	id INT NOT NULL AUTO_INCREMENT,
    wood INT NOT NULL,
    core INT NOT NULL,
    produtionDate DATE,
    PRIMARY KEY (id),
    FOREIGN KEY (wood) REFERENCES woods (id),
    FOREIGN KEY (core) REFERENCES cores (id)
);

CREATE TABLE mages (
	id INT NOT NULL AUTO_INCREMENT,
    name VARCHAR(256),
    wand INT UNIQUE,
    supervisor INT,
    PRIMARY KEY (id),
    FOREIGN KEY (wand) REFERENCES wands (id),
    FOREIGN KEY (supervisor) REFERENCES mages (id)
);

CREATE TABLE spells (
	id INT NOT NULL AUTO_INCREMENT,
    incantation VARCHAR(1024),
    PRIMARY KEY (id)
);

CREATE TABLE mages_spells (
	mage INT NOT NULL,
    spell INT NOT NULL,
    PRIMARY KEY (mage, spell),
    FOREIGN KEY (mage) REFERENCES mages (id),
    FOREIGN KEY (spell) REFERENCES spells (id)
    
);
CREATE TABLE spell_books(
id INT NOT NULL AUTO_INCREMENT,
title VARCHAR(256),
author VARCHAR(256),
publish_date DATE,
PRIMARY KEY (id)
);
CREATE TABLE spells_books (
	spell_book INT NOT NULL,
    spell INT NOT NULL,
    PRIMARY KEY (spell_book, spell),
    FOREIGN KEY (spell_book) REFERENCES spell_books (id),
    FOREIGN KEY (spell) REFERENCES spells (id)
    
);


INSERT INTO woods (name, toughness) VALUES
	('Beechwood', 50),
    ('Maple', 60),
    ('Ebony', 30),
    ('Holly', 40);

INSERT INTO cores (name, power, consistency) VALUES
	('Dragon heartstring', 100, 50),
    ('Phoenix feather', 75, 75),
    ('Unicorn hair', 50, 100);

INSERT INTO spells (incantation) VALUES
	('Silencion'),
    ('Accio'),
    ('Lumos'),
    ('Alohomora'),
    ('Expecto Patronum');

INSERT INTO wands (wood, core,produtionDate) VALUES (
	(SELECT id FROM woods WHERE name = 'Beechwood'),
    (SELECT id from cores WHERE name = 'Dragon heartstring'),
	'1893-01-23');
INSERT INTO mages (name, wand) VALUES ('Bojan Letvin', (SELECT LAST_INSERT_ID()));

update mages set supervisor =id where name =('Bojan Letvin') ;


INSERT INTO wands (wood, core,produtionDate) VALUES (
	(SELECT id FROM woods WHERE name = 'Holly'),
    (SELECT id from cores WHERE name = 'Phoenix feather'),
'1912-11-05'
);
INSERT INTO mages (name, wand) VALUES ('Zhivko Bernatsky', (SELECT LAST_INSERT_ID()));

update mages set supervisor =id where name =('Zhivko Bernatsky') ;

INSERT INTO wands (wood, core,produtionDate) VALUES (
	(SELECT id FROM woods WHERE name = 'Maple'),
    (SELECT id from cores WHERE name = 'Unicorn hair'),
'1743-05-13'
);
INSERT INTO mages (name, wand) VALUES ('Blagoja Petrovics', (SELECT LAST_INSERT_ID()));

update mages set supervisor =id where name =('Blagoja Petrovics') ;

INSERT INTO wands (wood, core,produtionDate) VALUES (
	(SELECT id FROM woods WHERE name = 'Maple'),
    (SELECT id from cores WHERE name = 'Unicorn hair'),
'0691-09-05'
);
INSERT INTO mages (name, wand) VALUES ('Jaropluk Yablonsky', (SELECT LAST_INSERT_ID()));

update mages set supervisor =id where name =('Jaropluk Yablonsky') ;

INSERT INTO spell_books (title, author, publish_date) VALUES 
('The Book of Aeris','Aeris','0234-01-20'),
('The Book of Elleshar','Elleshar','0306-01-23'),
('The Book of Loynis','Loynis','0305-01-23'),
('The Book of Septienna','Septienna','0443-01-23')
;
INSERT INTO spells_books (spell_book, spell) VALUES  
    ((SELECT id FROM spell_books WHERE author ='Aeris'),
     (SELECT id FROM spells WHERE incantation = 'Expecto Patronum')),
	((SELECT id FROM spell_books WHERE author ='Aeris'),
	 (SELECT id FROM spells WHERE incantation = 'Silencion')),
	((SELECT id FROM spell_books WHERE author ='Aeris'),
	 (SELECT id FROM spells WHERE incantation = 'Accio')),
	((SELECT id FROM spell_books WHERE author ='Aeris'),
	 (SELECT id FROM spells WHERE incantation = 'Alohomora')),


    ((SELECT id FROM spell_books WHERE author ='Elleshar'),
	 (SELECT id FROM spells WHERE incantation = 'Expecto Patronum')),
    ((SELECT id FROM spell_books WHERE author ='Elleshar'),
     (SELECT id FROM spells WHERE incantation = 'Alohomora')),
	((SELECT id FROM spell_books WHERE author ='Elleshar'),
     (SELECT id FROM spells WHERE incantation = 'Silencion')),
	((SELECT id FROM spell_books WHERE author ='Elleshar'),
     (SELECT id FROM spells WHERE incantation = 'Lumos')),

    ((SELECT id FROM spell_books WHERE author ='Loynis'),
     (SELECT id FROM spells WHERE incantation = 'Accio')),
    ((SELECT id FROM spell_books WHERE author ='Loynis'),
	 (SELECT id FROM spells WHERE incantation = 'Alohomora')),
	((SELECT id FROM spell_books WHERE author ='Loynis'),
	 (SELECT id FROM spells WHERE incantation = 'Expecto Patronum')),
	((SELECT id FROM spell_books WHERE author ='Loynis'),
      (SELECT id FROM spells WHERE incantation = 'Lumos')),

    ((SELECT id FROM spell_books WHERE author ='Septienna'),
     (SELECT id FROM spells WHERE incantation = 'Lumos')),
	((SELECT id FROM spell_books WHERE author ='Septienna'),
     (SELECT id FROM spells WHERE incantation = 'Silencion')),
    ((SELECT id FROM spell_books WHERE author ='Septienna'),
	 (SELECT id FROM spells WHERE incantation = 'Expecto Patronum')),
	((SELECT id FROM spell_books WHERE author ='Septienna'),
	 (SELECT id FROM spells WHERE incantation = 'Accio'));


INSERT INTO mages_spells (mage, spell) VALUES
	((SELECT id FROM mages WHERE name = 'Bojan Letvin'),
     (SELECT id FROM spells WHERE incantation = 'Expecto Patronum')),
	((SELECT id FROM mages WHERE name = 'Bojan Letvin'),
     (SELECT id FROM spells WHERE incantation = 'Silencion')),
    ((SELECT id FROM mages WHERE name = 'Zhivko Bernatsky'),
     (SELECT id FROM spells WHERE incantation = 'Expecto Patronum')),
    ((SELECT id FROM mages WHERE name = 'Zhivko Bernatsky'),
     (SELECT id FROM spells WHERE incantation = 'Accio')),
    ((SELECT id FROM mages WHERE name = 'Blagoja Petrovics'),
     (SELECT id FROM spells WHERE incantation = 'Expecto Patronum')),
    ((SELECT id FROM mages WHERE name = 'Blagoja Petrovics'),
     (SELECT id FROM spells WHERE incantation = 'Alohomora')),
    ((SELECT id FROM mages WHERE name = 'Jaropluk Yablonsky'),
     (SELECT id FROM spells WHERE incantation = 'Expecto Patronum')),
    ((SELECT id FROM mages WHERE name = 'Jaropluk Yablonsky'),
     (SELECT id FROM spells WHERE incantation = 'Alohomora'));


